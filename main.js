const puppeteer = require("puppeteer");
const express = require("express");
const socketio = require("socket.io");
const http = require("http");
const command = require("./command");

require("dotenv").config();
const { PORT, HOST } = process.env;

const app = express();

app.use(express.static("public"));

const server = http.createServer( app );
const sServer= socketio( server );

const controlModel = [ "slave", "master" ];
var users = new Object();

server.listen( PORT, HOST, ()=>{
  console.log( "Server activate" );
  //TODO: with enviroment RESTART=true, relaunch browser
  if( process.env.CLIENT == 1 ){
    ( async () => {
      // test audio     
      // const browser = await puppeteer.launch({
      //   ignoreDefaultArgs: ['--mute-audio'],
      // });
      // const page = await browser.newPage();
      // await page.goto('http://www.noiseaddicts.com/free-samples-mp3/?id=2544');
      // await page.click('span.map_play');
      // audio end

      const browser = await puppeteer.launch({
        headless: process.env.INTERFACE==1?false:true,
        ignoreDefaultArgs: ['--mute-audio'],
        args:[
          process.env.KIOSK==1?"--kiosk":"",
          '--autoplay-policy=no-user-gesture-required'
        ],
        defaultViewport: null,
      });

      
      const page = (await browser.pages())[0];
      browser.on("disconnected", serviceClosed);
      await page.goto(`http://${HOST}:${PORT}/player.html`);
      
      // .env PASSCODE empty for random  
      await page.type("#login_code", process.env.PASSCODE);
      await page.click('#login_btn');
      // await page.click('iframe');
      // await page.click('iframe');
    } )();
  }
} );


// https://socket.io/docs/v3/client-api/ next time using "manager.socket"
sServer.on( "connection", ( client )=>{
  client.on("register", ( data )=>{
    let result = false;
    console.log( client.id, "connecting", data );
    if( (typeof data) != "object" )return;
    
    if( (typeof data.model) == "number" && 
        (typeof data.key)   == "string" && 
        /[0-9a-f]{6}/.test( data.key ) 
      ){
      
      console.log(`new ${controlModel[data.model]} join`);
      if( users[ data.key ] == undefined ){
        users[ data.key ] = {
          master: {},
          slave:  {}
        };
      }

      let model = controlModel[ data.model ];
      if( model != undefined ){
        client.key = data.key;
        client.model = model;
        users[ data.key ][ model ][ client.id ] = client;
        console.log( users );
        result = true;
      }

    }

    client.emit("register", { result });
  });
  
  client.on("control", (data)=>{
    let cmd = command[ data.method ];

    if( (typeof data) != "object" || cmd == undefined 
    ){
      console.log("method has wrong");
      return;
    };
    
    console.log( users );
    if( !users[ client.key ] )
      return client.emit("control", { error:"group not found" });
    

    
    let slaves = users[ client.key ]["slave"];
    if(slaves!=undefined){
      for(let id in slaves){
        let slave = slaves[id];
        let json = cmd(data);
        console.log( json )
        slave.emit("control", json);
      }
    }
  })

  client.on("disconnect", ()=>{
    if( client.key !=undefined ){
      delete users[client.key][client.model][client.id];
      console.log( client.key, client.model, "disconnect" );
    }else{
      console.log("someone disconnect ");
    }
  })
} )

function serviceClosed(){
  if( process.env.RESTART )
    console.error("\x1b[31mError: This function not available \x1b[0m");
  console.log( "Browser closed" );
  server.close();
}