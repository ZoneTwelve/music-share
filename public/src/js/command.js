const method = {
  play:{
    "link":"string",
    "type":/\S+/,
    event:"onsubmit",
    method:(frame, data)=>{
      let url = data.v;
      switch(data.type){
        case "now":
          frame.list[0] = url;
        break;
        case "next":
          frame.list.splice(1, 0, url);
        break;
        case "last":
          frame.list.push( url );
        break;
        case "stop":
          frame.player.stopVideo();
        break;
        case "pause":
          frame.player.pauseVideo();
        break;
        case "play":
          frame.player.playVideo();
        break;
      }
      frame.playing();

    },
  },
  volume:{
    "value":"int",
    event:"onchange",
    method:( frame, data ) => {
      let { value } = data;
      frame.player.setVolume( value );
    }
  }
};
