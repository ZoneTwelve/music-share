const socket = io( location.origin );

var frame;
var serviceReady = false;


socket.on("control", (data)=>{
  let met = method[data.method];
  //data.list = list;
  console.log("data:");
  if( (typeof met.method) == "function" ){
    met.method( frame, data );
    console.log( data );
  }
  // frame.playing();
  //list = data.list;
  //apply( list );
});

var socketReady = false;


function beNewSlave( key ){
  key = key || (`000000${(~~(Math.random() * 0xffffff)).toString(16)}`).substr(-6);
  document.querySelector("#register").style.display = "none";
  document.querySelector("#main").style.display = "contents";

  document.querySelector("#passcode").innerText = key;

  console.log("join", key);
  socket.emit("register", {
    ua: navigator.userAgent,
    key,
    model: 0,
  });
}

window.onload = function( ){
  if(serviceReady==false)
    onYouTubePlayerAPIReady();
  document.querySelector('[name="beSlave"]').onsubmit = doc_createSlave;
}

function doc_createSlave(){
  let key = this.passcode.value;
  beNewSlave( key );

  return false; // stop submit
}

function onYouTubePlayerAPIReady() {
  serviceReady = true;
  frame = new youtubeWindow();
  socketReady = true;
}

function youtubeWindow(){
  this.player = new YT.Player('player', {
    height: '480',
    width: '100%',
    videoId: 'M7lc1UVf-VE',
    playerVars: {
      autoplay: 1,
    },
    events: {
      onReady: (event) => this.ready( event, this.player ),
      onStateChange: (event) => this.change( event, this.player ),
    }
  });
  this.list = [];
  this.current = null;
}

youtubeWindow.prototype.ready = function(event, player){ //
  player.stopVideo();
  player.setVolume(40)
  // player.target.setVolume(0);
  // player.playVideo();
  // console.log( player )
  // target.setVolume(0);

  //event.target.playVideo();
  //player.mute();
}

youtubeWindow.prototype.change = function(event, player){
  //console.log( event );
  if( event.data==0 ){
    console.log(this.list);
    this.list.splice(0, 1);
    this.playing( );
  }
  
}

youtubeWindow.prototype.stop = function(event){

}

youtubeWindow.prototype.playing = function(){
  if( this.current!=this.list[0] ){
    this.player.cueVideoById( this.list[0] );
    this.player.playVideo();
    this.current = this.list[0];
  }
}