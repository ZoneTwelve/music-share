const socket = io( location.origin );

socket.on("register", (data)=>{
  console.log( data );
  if( data.result ){
    showPanel( true );
  }else{
    alert( data );
  }
})

function showPanel(v = false){
  document.querySelector("#control").style.display  = v?"contents":"none";
  document.querySelector("#register").style.display = v?"none":"contents";
}

window.onload = function(){
  // showPanel( false );

  document.querySelector("#register").onsubmit = login;
  
  for(let el of document.querySelectorAll("form")){
    // console.log( el );
    console.log( el.name )
    if(method[el.name]){
      // el.addEventListener( method[ el.name ]['event'], (form) => {
      //     let formName = form.target.getAttribute("name");
      //     controlMethod( formName, form.target );
      //     return false;
      //   }
      // );
      el.onsubmit = (form) => {
        let formName = form.target.getAttribute("name");
        controlMethod( formName, form.target );
        return false;
      }
      // if(method[el.name]['event']=="onchange")
      //   el.onchange = (form) => {
      //     let formName = form.target.getAttribute("name");
      //     controlMethod( formName, form.target );
      //     return false;
      //   }
    }
  }
}

function controlMethod( target, form ){
  let json = new Object();
  json.method = target;
  for(let el in method[target]){
    if(el=="method" || el=="event")continue;
    console.log( "el", el );
    json[el] = ( form.elements.namedItem(el).value );
  }
  console.log( json );
  socket.emit("control", json);
}

function login( key ){
  console.log( "this", this );
  key = (typeof key=="string")? key : this.key.value;

  console.log( key );
  socket.emit("register", {
    ua: navigator.userAgent,
    model:1,
    key, 
  });

  return false;
}
