const URL = require("url");

module.exports = {
  play:( data ) => {
    let { type, link } = data;
    console.log( link );

    let url = (typeof link=='string') ? URL.parse( link, true ) : {};
    let domain = url.hostname;
    let res = new Object();
    res['method'] = "play";
    res['type'] = type;
    switch( domain ){
      case "youtu.be":
        res['v'] = url.pathname.substr(1, url.length);
      break;
      case "youtube.com":
      case "www.youtube.com":
        console.log( url.query.v );
        res['v'] = url.query.v;
        res['list'] = url.query.list;
      break;
      case null:
        res['v'] = url.pathname;
      break;
    }
    
    return res;
  },
  volume:( data ) => {
    let res = new Object();
    res['method'] = "volume";
    res['value']  = parseInt( data.value ); 
    return res;
  },

};